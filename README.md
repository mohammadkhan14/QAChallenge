Automation Setup Instruction

Documents contains instructions on all required pre requisite [For detailed screen shot refer readme.docx file]

1.	Java 8
2.	Apache Maven 3.1
3.	Environment variables
4.	Eclipse Kepler (IDE Tool)
5.	Cucumber plugin Installation
6.	TestNG Installation
7.	Download project from GIT
8.	Run

1.  Java
Download JDK 8 zip from below location
https://www.openlogic.com/openjdk-downloads?field_java_parent_version_target_id=416&field_operating_system_target_id=436&field_architecture_target_id=391&field_java_package_target_id=396

Once the zip folder is downloaded, extract it [No need for installation]
After the zip folder is extracted , we will setup JAVA_HOME and path in System variable under Environment Variables.


2.- Apache Maven 3.1
Download binary zip from https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.1.0/

Unzip content 


3.- Environment variables 
•	Navigate to System Properties
•	Click on the Environment Variables button
Update / Create the System variables with the correct Java and Maven directory path:

JAVA_HOME	C:\Program Files\Java\openlogic-openjdk-8u262-b10-windows-x64
M2_HOME	C:\apache-maven-3.1.0
PATH	C:\Program Files\Java\ openlogic-openjdk-8u262-b10-windows-x64\bin;C:\APACHE-MAVEN\apache-maven-3.1.0\bin;

Go to command prompt and type 
Java -version 
Mvn -version
We should see the version If the path is set correctly.

4.- Eclipse Kepler (IDE tool)
https://www.eclipse.org/downloads/packages/release/kepler/sr2-java8-patchesDownload eclipse-inst-win64.exe

Download the jar from the above url and unzip it.
Go to the extracted folder and navigate to eclipse folder and open using the exe.

Project Repository: https://gitlab.com/mohammadkhan14/QAChallenge.git

5.- Cucumber plugin 
•	Download Cucumber plugin from https://drive.google.com/file/d/1gW3Ekh7XblKHvt_RaUhtsMVJIZ2g-jJ6/view?usp=sharing
•	DO NOT UNZIP the downloaded plugin
•	Launch the Eclipse IDE and from Help menu, click “Install New Software”. 
•	You will see a dialog window, click “Add” button. 
•	Click on Archive and give the name as cucumber and select the downloaded plugins path.
•	Click Ok
•	 Click on Next. 
•	Click “I accept the terms of the license agreement” then click Finish. 
•	Let it install, it will take few seconds to complete. 
•	You may or may not encounter a Security warning, if in case you do just click OK. 
•	You are all done now, just Click Yes. 
   
6.- TestNG plugin
It is easy to install TestNG Eclipse Plugin, as it comes as a plugin for Eclipse IDE. Prerequisite for installing this plugin is your Internet connection should be up & running during installation of this plugin and Eclipse IDE should be installed in your computer. 
•	Launch the Eclipse IDE and from Help menu, click “Install New Software”. 
•	You will see a dialog window, click “Add” button.
•	Type name as you wish, lets take “TestNG” and type https://testng.org/testng-eclipse-update-site as location.  
•	Click OK.
•	You come back to the previous window but this time you must see TestNG option in the available software list. 
•	Just Click TestNG and press “Next” button.
•	Click on Next and wait for it to download the dependencies.
•	Click on I accept the terms of the license agreement
•	Click Finish and Install Anyway.
•	Restart Eclipse.


7. Download the project
•	Clone the repository to your local machine, from  https://gitlab.com/mohammadkhan14/QAChallenge.git
•	Import project to Eclipse  
•	Select Git
•	Select Clone URL and click Next  
•	Paste https://gitlab.com/mohammadkhan14/QAChallenge.git in URL field and click Next.
•	Click Next and Finish.
•	Wait for Maven Dependencies to get Updated
•	Verify if the project is open in the explorer
 

8. Run
•	Right Click on testing.xml and select Run As TestNG Suite.

Reports can be located from 
\QAChallenge\src\test\resources\AutomationSuite
 

 

