Feature: Add Product to card and login

  @RegressionTest
  Scenario Outline: Add Product to card and login
    Given User is in fashionette home page
    When I add "<Product>" to the cart from "<Section>"
    And I login using using "<UserName>" and "<Password>"
    And I navigate to cart
    Then Validate the cart to check if the items are added
    

    Examples: 
      | Section      | Product | UserName | Password  |
      | Handbags      | Prada    | QA@fashionette.de  |    !8Ntr*BM@!#G3VH       |
