package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import resources.functionalLibrary;

public class CartPage extends functionalLibrary {

	public CartPage() {
		PageFactory.initElements(functionalLibrary.driver, this);
	}

	@FindBy(xpath = "//div[@class='cart-item--name']")
	private WebElement productDescription;

	public WebElement getProductDescription() {
		return productDescription;
	}

	@FindBy(xpath = "//div[@class='cart-item--price']//span")
	private WebElement productPrice;

	public WebElement getProductPrice() {
		return productPrice;
	}

	@FindBy(xpath = "(//i[@class='icon icon--inline icon--cross'])[1]")
	private WebElement closeIcon;

	public WebElement getCloseIcon() {
		return closeIcon;
	}

	@FindBy(xpath = "//th[contains(text(),'Subtotal')]")
	private WebElement subtotalHeader;

	public WebElement getSubtotalHeader() {
		return subtotalHeader;
	}

	@FindBy(xpath = "//th[contains(text(),'Voucher')]")
	private WebElement voucherHeader;

	public WebElement getVoucherHeader() {
		return voucherHeader;
	}

	@FindBy(xpath = "//th[contains(text(),'Shipping')]")
	private WebElement shippingHeader;

	public WebElement getShipping() {
		return shippingHeader;
	}

	@FindBy(xpath = "//th[contains(text(),'Total')]")
	private WebElement totalHeader;

	public WebElement getTotalHeader() {
		return totalHeader;
	}

	@FindBy(xpath = "//th[contains(text(),'incl. 20% VAT')]")
	private WebElement taxHeader;

	public WebElement gettaxHeader() {
		return taxHeader;
	}

	@FindBy(xpath = "//span[@class='recommendations__hint__text']")
	private WebElement hintText;

	public WebElement getHintText() {
		return hintText;
	}

	@FindBy(xpath = "//th[contains(text(),'Voucher')]//following::A[contains(text(),'redeem')]")
	private WebElement redeemLink;

	public WebElement getRedeemLink() {
		return redeemLink;
	}

	@FindBy(xpath = "//th[contains(text(),'Voucher')]//i")
	private WebElement removeVoucher;

	public WebElement getRemoveVoucherCloseIcon() {
		return removeVoucher;
	}

	@FindBy(xpath = "//div[contains(text(),'YES')]")
	private WebElement yesButtonToRemoveVoucher;

	public WebElement getYesButtonToRemoveVoucher() {
		return yesButtonToRemoveVoucher;
	}

	@FindBy(xpath = "//th[contains(text(),'Voucher')]//following::td[contains(text(),'£0')]")
	private WebElement shippingAmount;

	public WebElement getShippingAMount() {
		return shippingAmount;
	}

	@FindBy(id = "cart__total")
	private WebElement cartTotal;

	public WebElement getCartTotal() {
		return cartTotal;
	}

	@FindBy(id = "cart__tax")
	private WebElement cartTax;

	public WebElement getCartTax() {
		return cartTax;
	}

	@FindBy(xpath = "//a[text()='(more details)']")
	private WebElement moreDetailsLink;

	public WebElement getMoreDetailsLink() {
		return moreDetailsLink;
	}

	@FindBy(id = "checkout-start")
	private WebElement continueShoppingButton;

	public WebElement getContinueShoppingButton() {
		return continueShoppingButton;
	}

	@FindBy(xpath = "//a[contains(text(),'Keep shopping at fashionette')]")
	private WebElement keepShoppingLink;

	public WebElement getkeepShoppingLink() {
		return keepShoppingLink;
	}

	@FindBy(xpath = "//input[@name='voucherCode']")
	private WebElement voucherCodeInput;

	public WebElement getVoucherCodeInput() {
		return voucherCodeInput;
	}

	@FindBy(xpath = "//button[@type='submit' and contains(text(),'redeem')]")
	private WebElement redeemButton;

	public WebElement getRedeemButton() {
		return redeemButton;
	}

	public WebElement getVoucherApplied(String discountCode, String discountAmount) {
		WebElement dynamicElement = functionalLibrary.driver.findElement(
				By.xpath("//span[@data-code='" + discountCode + "' and contains(text(),'" + discountAmount + "')]"));
		return dynamicElement;
	}
}