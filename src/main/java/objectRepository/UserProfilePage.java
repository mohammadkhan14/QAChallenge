package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import resources.functionalLibrary;

public class UserProfilePage extends functionalLibrary {

	public UserProfilePage() {
		PageFactory.initElements(functionalLibrary.driver, this);
	}

	@FindBy(xpath = "//a[contains(text(),'Personal data')]")
	private WebElement personalDataSection;

	public WebElement getPersonalDataSection() {
		return personalDataSection;
	}

	@FindBy(xpath = "//div[contains(text(),'Edit')]")
	private WebElement editButtonPersonalDetailsSection;

	public WebElement getEditButtonPersonalDetailsSection() {
		return editButtonPersonalDetailsSection;
	}

	@FindBy(xpath = "//input[@name='firstName']")
	private WebElement firstNameInput;

	public WebElement getFirstNameInput() {
		return firstNameInput;
	}

	@FindBy(xpath = "//input[@name='lastName']")
	private WebElement lastNameInput;

	public WebElement getLastNameInput() {
		return lastNameInput;
	}

	@FindBy(xpath = "//div[contains(text(),'Save')]")
	private WebElement saveButton;

	public WebElement getSaveButton() {
		return saveButton;
	}

	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	private WebElement logoutButton;

	public WebElement getLogoutButton() {
		return logoutButton;
	}

	public WebElement getGuestDetails(String firstName, String lastName) {
		WebElement dynamicElement = functionalLibrary.driver
				.findElement(By.xpath("//div[contains(text(),'" + firstName + " " + lastName + "')]"));
		return dynamicElement;
	}

}