package objectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import resources.functionalLibrary;

public class LoginPage extends functionalLibrary {

	public LoginPage() {
		PageFactory.initElements(functionalLibrary.driver, this);
	}

	@FindBy(xpath = "//input[@name='email']")
	private WebElement userNameInput;

	public WebElement getUserNameInput() {
		return userNameInput;
	}

	@FindBy(xpath = "//input[@name='password']")
	private WebElement passwordInput;

	public WebElement getPasswordInput() {
		return passwordInput;
	}

	@FindBy(xpath = "//button[@type='submit' and contains(text(),'Login')]")
	private WebElement loginButton;

	public WebElement getloginButton() {
		return loginButton;
	}

}