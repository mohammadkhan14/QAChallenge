package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import resources.functionalLibrary;

public class HomePage extends functionalLibrary {

	public HomePage() {
		PageFactory.initElements(functionalLibrary.driver, this);
	}

	@FindBy(id = "uc-btn-accept-banner")
	private WebElement acceptCookie;

	public WebElement getAcceptCookie() {
		return acceptCookie;
	}

	@FindBy(xpath = "//a[@class='header__logo img-background']")
	private WebElement mainLogo;

	public WebElement getMainLogo() {
		return mainLogo;
	}

	public WebElement getProductSection(String identifier) {
		WebElement dynamicElement = functionalLibrary.driver.findElement(By.xpath("//p[text()='" + identifier + "']"));
		return dynamicElement;
	}

//    public WebElement getProductSection(String identifier) {
//        WebElement dynamicElement = functionalLibrary.driver.findElement(By.xpath("//div[contains(text(),'"+identifier+"')]"));
//        return dynamicElement;
//    }

	public WebElement getProduct(String identifier) {
		WebElement dynamicElement = functionalLibrary.driver.findElement(By.xpath(
				"//div[contains(text(),'" + identifier + "')]//preceding::div[@class='product--list__item__image']"));
		return dynamicElement;
	}

	@FindBy(xpath = "(//div[contains(text(),'Add to cart')])[2]")
	private WebElement addToCart;

	public WebElement getAddToCart() {
		return addToCart;
	}

	@FindBy(xpath = "//a[@title='Login']")
	private WebElement loginButton;

	public WebElement getloginButton() {
		return loginButton;
	}

	@FindBy(css = "a.header__user-icon[data-id=\"user account\"]")
	private WebElement userIcon;

	public WebElement getUserIcon() {
		return userIcon;
	}

	@FindBy(xpath = "//div[@class='product-details__description__price--special font-size--h1 text__weight--semi-bold']")
	private WebElement productPrice;

	public WebElement getProductPrice() {
		return productPrice;
	}

	@FindBy(xpath = "//small[@class='product-details__description__name text__weight--normal']")
	private WebElement productDescription;

	public WebElement getProductDescription() {
		return productDescription;
	}

	@FindBy(xpath = "//span[contains(text(),'Go to Cart')]")
	private WebElement goToCartLink;

	public WebElement getGoToCartLink() {
		return goToCartLink;
	}
}