package runner;

import java.io.File;
import java.io.IOException;
import org.joda.time.LocalDateTime;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import resources.ExtentCucumberFormatter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.TestNGCucumberRunner;
import resources.functionalLibrary;
import resources.helperClass;

@CucumberOptions(features = { "Feature" }, tags = { "~@OutOfScope" }, plugin = { "resources.ExtentCucumberFormatter:",
		"json:src/Cucumber.json", "rerun:src/rerun.txt" }, glue = "stepdefinition")

public class TestRunner {

	public TestRunner() {

	}

	public static void main(String args[]) throws Throwable {
		String features = "";
		for (int i = 0; i < args.length; i++) {
			features = features + args[i];
			features = features + " ";
		}

		System.setProperty("cucumber.options", features);
		TestRunner commandLineRunner = new TestRunner();
		commandLineRunner.beforeClass();
		commandLineRunner.runCukes();
		commandLineRunner.endReportsAndQuitBrowser();
	}

	@Test
	public void runCukes() {
		new TestNGCucumberRunner(getClass()).runCukes();
	}

	@BeforeClass
	public void beforeClass() throws Throwable {
		helperClass helper = new helperClass(); // url is read from helper class
		LocalDateTime now = LocalDateTime.now(); // To save the test report with the date and time
		int year = now.getYear();
		int month = now.getMonthOfYear();
		int day = now.getDayOfMonth();
		int hour = now.getHourOfDay();
		int minute = now.getMinuteOfHour();
		int second = now.getSecondOfMinute();
		// Append the retrieved month year date to the generated report file
		String snewFilename1 = year + "_" + month + "_" + day + "_" + hour + "_" + minute + "_" + second;
		String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\AutomationSuite\\Report_"
				+ snewFilename1 + ".html";
		// Report will be stored in --> Demo\src\test\resources\AutomationSuite folder
		File file = new File(filePath);
		String url;
		ExtentCucumberFormatter.setExtentHtmlReport(file);
		url = helper.getURL(); // url fetched from /Demo/testData/data.properties
		functionalLibrary.driverInit(); // Initialize the chrome driver
		functionalLibrary.driver.get(url); // Load the url before the test beguns
	}

	@AfterClass
	public void endReportsAndQuitBrowser() throws IOException {
		functionalLibrary.quitBrowser(); // after the execution of feature file, quit the browser
		Runtime.getRuntime().exec("taskkill /F /IM ChromeDriver.exe");
	}

}
