package resources;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import resources.Reporter;

public class functionalLibrary {

	public static WebDriver driver;

	/*
	 * Initialize the chrome web driver and start it in maximized mode Implicit wait
	 * is set at 5 secs
	 */

	public static WebDriver driverInit() {

		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		ChromeOptions o = new ChromeOptions();
		o.addArguments("disable-extensions");
		o.addArguments("--start-maximized");
		driver = new ChromeDriver(o);
		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		return driver;
	}

	/*
	 * Quit browser should close the driver after the execution
	 * 
	 */

	public static void quitBrowser() {
		try {
			driver.quit();
			Reporter.addStepLogPass("Browser quitsuccessfully");
		} catch (Exception e) {
			Reporter.addStepLogInfo("Browser didn not quit");
		}
	}

	/*
	 * clickjs is used to simulate click using javascriptExecutor This function is
	 * used when normal selenium click fails
	 */

	public static void clickjs(WebElement element) {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
			Reporter.addStepLogPass(element.toString() + "element is clicked successfully");
		} catch (Exception e) {
			Reporter.addStepLogInfo(element.toString() + "element is not clicked" + e.getMessage());
		}
	}

	/*
	 * This method will wait for element to be visible explicitly The max time will
	 * be 30 sec, if the element is not visible in 30 sec This will fail
	 */

	public static void waitForElementVisibility(WebElement element) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(5, TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	/*
	 * This method will wait for element to be Clickable explicitly The max time
	 * will be 30 sec, if the element is not visible in 30 sec This will fail
	 */

	public static void waitForElementClickable(WebElement element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 4);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			Reporter.addStepLogInfo("Element is clickable at this point");
		} catch (Exception e) {
			Reporter.addStepLogInfo("Element is not clickable at this point exception");
		}
	}

	public void scrollToView(WebElement element) {

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

	}

	public void jsScrollPageDownPartial() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,400)", "");
		} catch (Exception e) {
			Reporter.addStepLogInfo("page is not scrolled down ");
		}
	}

	public static void jsWaitForPageLoad() {
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		String pageReadyState = (String) ((JavascriptExecutor) driver).executeScript("return document.readyState");
		while (!pageReadyState.equals("complete")) {
			pageReadyState = (String) ((JavascriptExecutor) driver).executeScript("return document.readyState");
		}

	}

	public static String cleanTextContent(String text) {
		// strips off all non-ASCII characters
		text = text.replaceAll("[^\\x00-\\x7F]", "");

		// erases all the ASCII control characters
		text = text.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "");

		// removes non-printable characters from Unicode
		text = text.replaceAll("\\p{C}", "");

		// remove , from the amount
		text = text.replaceAll("\\,", "");

		return text.trim();
	}
}
