package resources;

import java.io.FileInputStream;

import java.io.IOException;
import java.util.Properties;

public class helperClass {

	public static final String TEMPLATE_FILE_CONTEXT = "data.properties";

	public String getURL() {
		return getEnvironmentDetails().getProperty("URL");
	}

	public Properties getEnvironmentDetails() {

		Properties configProperties = new Properties();
		try {
			configProperties.load(new FileInputStream(System.getProperty("user.dir") + "/testData/data.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return configProperties;

	}

}
