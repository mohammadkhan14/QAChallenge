package resources;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import gherkin.formatter.Formatter;
import gherkin.formatter.Reporter;
import gherkin.formatter.model.Background;
import gherkin.formatter.model.DataTableRow;
import gherkin.formatter.model.Examples;
import gherkin.formatter.model.ExamplesTableRow;
import gherkin.formatter.model.Feature;
import gherkin.formatter.model.Match;
import gherkin.formatter.model.Result;
import gherkin.formatter.model.Scenario;
import gherkin.formatter.model.ScenarioOutline;
import gherkin.formatter.model.Step;
import gherkin.formatter.model.Tag;

/**
 * A cucumber based reporting listener which generates the Extent Report
 */
public class ExtentCucumberFormatter implements Reporter, Formatter {

	private static ExtentReports extentReports;
	private static ExtentHtmlReporter htmlReporter;
	private static ThreadLocal<ExtentTest> featureTestThreadLocal = new InheritableThreadLocal<ExtentTest>();
	private static ThreadLocal<ExtentTest> scenarioOutlineThreadLocal = new InheritableThreadLocal<ExtentTest>();
	static ThreadLocal<ExtentTest> scenarioThreadLocal = new InheritableThreadLocal<ExtentTest>();
	private static ThreadLocal<LinkedList<Step>> stepListThreadLocal = new InheritableThreadLocal<LinkedList<Step>>();
	static ThreadLocal<ExtentTest> stepTestThreadLocal = new InheritableThreadLocal<ExtentTest>();
	private boolean scenarioOutlineFlag;
	static File csvFile;
	static FileWriter writer;

	public ExtentCucumberFormatter(File file) {
		setExtentHtmlReport(file);
		setExtentReport();
		stepListThreadLocal.set(new LinkedList<Step>());
		scenarioOutlineFlag = false;
	}

	public static void setExtentHtmlReport(File file) {

		String CsvFile = file.getAbsolutePath().replaceAll(".html", "") + ".csv";
		System.out.println(CsvFile);

		if (htmlReporter != null) {
			return;
		}

		if (!file.exists()) {
			file.getParentFile().mkdirs();

			try {
				writer = new FileWriter(CsvFile);
			} catch (IOException e) {

				e.printStackTrace();
			}
		}
		htmlReporter = new ExtentHtmlReporter(file);
	}

	static ExtentHtmlReporter getExtentHtmlReport() {
		return htmlReporter;
	}

	private static void setExtentReport() {
		if (extentReports != null) {
			return;
		}
		extentReports = new ExtentReports();
		extentReports.attachReporter(htmlReporter);
		// uncomment the below step when DB report is needed
		// extentReports.attachReporter(htmlReporter,getExtentXReporter());
	}

	static ExtentReports getExtentReport() {
		return extentReports;
	}

	public void syntaxError(String state, String event, List<String> legalEvents, String uri, Integer line) {

	}

	public void uri(String uri) {

	}

	public void feature(Feature feature) {
		featureTestThreadLocal.set(getExtentReport().createTest(feature.getName()));
		ExtentTest test = featureTestThreadLocal.get();

		try {
			writer.write("\n" + feature.getName());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Tag tag : feature.getTags()) {
			test.assignCategory(tag.getName());
		}
	}

	public void scenarioOutline(ScenarioOutline scenarioOutline) {
		scenarioOutlineFlag = true;
		ExtentTest node = featureTestThreadLocal.get()
				.createNode(scenarioOutline.getKeyword() + ": " + scenarioOutline.getName());
		scenarioOutlineThreadLocal.set(node);
	}

	public void examples(Examples examples) {
		ExtentTest test = scenarioOutlineThreadLocal.get();

		String[][] data = null;
		List<ExamplesTableRow> rows = examples.getRows();
		int rowSize = rows.size();
		for (int i = 0; i < rowSize; i++) {
			ExamplesTableRow examplesTableRow = rows.get(i);
			List<String> cells = examplesTableRow.getCells();
			int cellSize = cells.size();
			if (data == null) {
				data = new String[rowSize][cellSize];
			}
			for (int j = 0; j < cellSize; j++) {
				data[i][j] = cells.get(j);
			}
		}
		test.info(MarkupHelper.createTable(data));
	}

	public void startOfScenarioLifeCycle(Scenario scenario) {
		if (scenarioOutlineFlag) {
			scenarioOutlineFlag = false;
		}

		ExtentTest scenarioNode;
		if (scenarioOutlineThreadLocal.get() != null
				&& scenario.getKeyword().trim().equalsIgnoreCase("Scenario Outline")) {
			scenarioNode = scenarioOutlineThreadLocal.get().createNode("Scenario: " + scenario.getName());
		} else {
			scenarioNode = featureTestThreadLocal.get().createNode("Scenario: " + scenario.getName());
		}
		// Reporter.addStepLogInfo("Executing->"+scenario.getName());
		for (Tag tag : scenario.getTags()) {
			if (!tag.getName().matches("\\@\\w*[0-9]")) {
				scenarioNode.assignCategory(tag.getName());
				System.out.println(tag.getName());
				System.out.println(scenario.getLine());
				// System.out.println(scenario.getName());

				try {
					writer.write("," + tag.getName());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		scenarioThreadLocal.set(scenarioNode);
	}

	public void background(Background background) {

	}

	public void scenario(Scenario scenario) {

	}

	public void step(Step step) {
		if (scenarioOutlineFlag) {
			return;
		}
		stepListThreadLocal.get().add(step);
	}

	public void endOfScenarioLifeCycle(Scenario scenario) {

	}

	public void done() {
		getExtentReport().flush();
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close() {

	}

	public void eof() {

	}

	public void before(Match match, Result result) {

	}

	public void result(Result result) {
		if (scenarioOutlineFlag) {
			return;
		}

		if (Result.PASSED.equals(result.getStatus())) {
			stepTestThreadLocal.get().pass(Result.PASSED);
			try {
				writer.write(",Pass");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (Result.FAILED.equals(result.getStatus())) {
			stepTestThreadLocal.get().fail(result.getError());
			stepTestThreadLocal.get().fail(result.getStatus());
			try {
				writer.write(",Fail");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (Result.SKIPPED.equals(result)) {
			stepTestThreadLocal.get().skip(Result.SKIPPED.getStatus());
			try {
				writer.write(",Skipped");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (Result.UNDEFINED.equals(result)) {
			stepTestThreadLocal.get().skip(Result.UNDEFINED.getStatus());
			try {
				writer.write(",Undefined");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void after(Match match, Result result) {

	}

	public void match(Match match) {
		Step step = stepListThreadLocal.get().poll();
		String data[][] = null;
		if (step.getRows() != null) {
			List<DataTableRow> rows = step.getRows();
			int rowSize = rows.size();
			for (int i = 0; i < rowSize; i++) {
				DataTableRow dataTableRow = rows.get(i);
				List<String> cells = dataTableRow.getCells();
				int cellSize = cells.size();
				if (data == null) {
					data = new String[rowSize][cellSize];
				}
				for (int j = 0; j < cellSize; j++) {
					data[i][j] = cells.get(j);
				}
			}
		}

		ExtentTest scenarioTest = scenarioThreadLocal.get();
		ExtentTest stepTest = scenarioTest.createNode(step.getKeyword() + step.getName());

		if (data != null) {
			Markup table = MarkupHelper.createTable(data);
			stepTest.info(table);
		}

		stepTestThreadLocal.set(stepTest);
	}

	public void embedding(String mimeType, byte[] data) {

	}

	public void write(String text) {

	}

	
}
