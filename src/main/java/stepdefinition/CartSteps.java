package stepdefinition;

import org.testng.Assert;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import objectRepository.CartPage;
import resources.functionalLibrary;

public class CartSteps extends functionalLibrary {

	CartPage cartPage = new CartPage();
	HomePageSteps homePageSteps = new HomePageSteps();

	@Then("^Validate the cart to check if the items are added$")
	public void validate_the_cart_to_check_if_the_items_are_added() throws Throwable {

		waitForElementVisibility(cartPage.getProductDescription());

		try {
			cartPage.getRemoveVoucherCloseIcon().click();
			cartPage.getYesButtonToRemoveVoucher().click();
		} catch (Exception e) {

		}

		Assert.assertEquals(cartPage.getProductDescription().getText(), HomePageSteps.productDescription);
		Assert.assertEquals(cartPage.getProductPrice().getText(), HomePageSteps.productPrice);
		Assert.assertTrue(cartPage.getCloseIcon().isDisplayed());
		Assert.assertTrue(cartPage.getSubtotalHeader().isDisplayed());
		Assert.assertTrue(cartPage.getVoucherHeader().isDisplayed());
		Assert.assertTrue(cartPage.getShipping().isDisplayed());
		Assert.assertTrue(cartPage.getTotalHeader().isDisplayed());
		Assert.assertTrue(cartPage.gettaxHeader().isDisplayed());
		Assert.assertTrue(cartPage.getHintText().isDisplayed());
		Assert.assertTrue(cartPage.getRedeemLink().isDisplayed());
		Assert.assertTrue(cartPage.getShippingAMount().isDisplayed());
		Assert.assertTrue(cartPage.getCartTotal().isDisplayed());
		Assert.assertEquals(cartPage.getCartTotal().getText(), HomePageSteps.productPrice);
		Assert.assertTrue(cartPage.getCartTax().isDisplayed());
		Assert.assertTrue(cartPage.getMoreDetailsLink().isDisplayed());
		Assert.assertTrue(cartPage.getkeepShoppingLink().isDisplayed());
		clickjs(cartPage.getCloseIcon()); // Remove the item from card

	}

	@When("^I apply \"([^\"]*)\" for \"([^\"]*)\"$")
	public void i_apply_for(String voucherCode, String discountAmount) throws Throwable {
		try {
			(cartPage.getRemoveVoucherCloseIcon()).click();
			(cartPage.getYesButtonToRemoveVoucher()).click();
		} catch (Exception e) {

		}
		waitForElementVisibility(cartPage.getRedeemLink());
		clickjs(cartPage.getRedeemLink());
		clickjs(cartPage.getVoucherCodeInput());
		cartPage.getVoucherCodeInput().sendKeys(voucherCode);
		clickjs(cartPage.getRedeemButton());
		Assert.assertTrue(cartPage.getVoucherApplied(voucherCode, discountAmount).isDisplayed());

	}

	@Then("^Verify if the discount \"([^\"]*)\" is applied to the total$")
	public void verify_if_the_discount_is_applied_to_the_total(String discountApplied) throws Throwable {

		int total = Integer.parseInt(functionalLibrary.cleanTextContent(cartPage.getProductPrice().getText()))
				- Integer.parseInt(discountApplied);

		Assert.assertEquals(functionalLibrary.cleanTextContent(cartPage.getCartTotal().getText()),
				String.valueOf(total));

	}

}