package stepdefinition;

import org.testng.Assert;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import objectRepository.UserProfilePage;
import resources.functionalLibrary;

public class UserProfileSteps extends functionalLibrary {

	UserProfilePage userProfile = new UserProfilePage();

	@When("^I navigate to personal data section$")
	public void i_navigate_to_personal_data_section() throws Throwable {
		waitForElementVisibility(userProfile.getPersonalDataSection());
		clickjs(userProfile.getPersonalDataSection());
	}

	@When("^I click on edit under personal guest details section$")
	public void i_click_on_edit_under_personal_guest_details_section() throws Throwable {
		waitForElementVisibility(userProfile.getEditButtonPersonalDetailsSection());
		clickjs(userProfile.getEditButtonPersonalDetailsSection());
	}

	@When("^I update the guest \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_update_the_guest_and(String firstName, String lastName) throws Throwable {
		waitForElementVisibility(userProfile.getFirstNameInput());
		userProfile.getFirstNameInput().clear();
		userProfile.getFirstNameInput().sendKeys(firstName);
		userProfile.getLastNameInput().clear();
		userProfile.getLastNameInput().sendKeys(lastName);
	}

	@When("^I click on save under personal guest details section$")
	public void i_click_on_save_under_personal_guest_details_section() throws Throwable {
		waitForElementVisibility(userProfile.getSaveButton());
		clickjs(userProfile.getSaveButton());
	}

	@When("^I logout after updating the guest information$")
	public void i_logout_after_updating_the_guest_information() throws Throwable {
		waitForElementVisibility(userProfile.getLogoutButton());
		clickjs(userProfile.getLogoutButton());
	}

	@Then("^Validate the updated \"([^\"]*)\" and \"([^\"]*)\" are saved$")
	public void validate_the_updated_and_are_saved(String expectedFirstName, String expectedLastName) throws Throwable {

		waitForElementVisibility(userProfile.getGuestDetails(expectedFirstName, expectedLastName));
		Assert.assertTrue(userProfile.getGuestDetails(expectedFirstName, expectedLastName).isDisplayed());

	}

}