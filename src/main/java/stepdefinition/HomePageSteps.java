package stepdefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import objectRepository.HomePage;
import objectRepository.LoginPage;
import resources.functionalLibrary;

public class HomePageSteps extends functionalLibrary {

	HomePage homePage = new HomePage();
	LoginPage loginPage = new LoginPage();
	static String productPrice;
	static String productDescription;

	@Given("^User is in fashionette home page$")
	public void user_is_in_fashionette_home_page() throws Throwable {
		try {
			waitForElementVisibility(homePage.getAcceptCookie());
			homePage.getAcceptCookie().click();
		} catch (Exception e) {
			homePage.getMainLogo().click();
		}

	}

	@When("^I add \"([^\"]*)\" to the cart from \"([^\"]*)\"$")
	public void i_add_to_the_cart_from(String product, String section) throws Throwable {
		scrollToView(homePage.getProductSection(section));
		waitForElementVisibility(homePage.getProductSection(section));
		clickjs(homePage.getProductSection(section));
		jsScrollPageDownPartial();
		jsWaitForPageLoad();
		try {
			waitForElementVisibility(homePage.getProduct(product));
		} catch (Exception e) {
			waitForElementVisibility(homePage.getProduct(product));
		}
		clickjs(homePage.getProduct(product));
		try {

			productPrice = homePage.getProductPrice().getText();
			productDescription = homePage.getProductDescription().getText();

		} catch (Exception e) {
			productPrice = homePage.getProductPrice().getText();
			productDescription = homePage.getProductDescription().getText();
		}
		waitForElementVisibility(homePage.getAddToCart());
		clickjs(homePage.getAddToCart());
	}

	@When("^I navigate to cart$")
	public void i_navigate_to_cart() throws Throwable {
		waitForElementVisibility(homePage.getGoToCartLink());
		clickjs(homePage.getGoToCartLink());
	}

}