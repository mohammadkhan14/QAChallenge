package stepdefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import objectRepository.HomePage;
import objectRepository.LoginPage;
import objectRepository.UserProfilePage;
import resources.functionalLibrary;

public class LoginPageSteps extends functionalLibrary {

	HomePage homePage = new HomePage();
	LoginPage loginPage = new LoginPage();
	UserProfilePage userProfile = new UserProfilePage();

	@Given("^User logs out if already logged in$")
	public void user_logs_out_if_already_logged_in() throws Throwable {
		try {
			homePage.getMainLogo().click();
		} catch (Exception e) {
			clickjs(homePage.getMainLogo());
		}

		try {
			waitForElementVisibility(homePage.getUserIcon());
			homePage.getUserIcon().click();
			waitForElementVisibility(userProfile.getLogoutButton());
			clickjs(userProfile.getLogoutButton());
		} catch (Exception e) {

		}
	}

	@When("^I login using using \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_login_using_using_and(String UserName, String Password) throws Throwable {

		waitForElementVisibility(homePage.getloginButton());
		try {
			homePage.getloginButton().click();
		} catch (Exception e) {
			clickjs(homePage.getloginButton());
		}

		waitForElementVisibility(loginPage.getUserNameInput());
		clickjs(loginPage.getUserNameInput());
		loginPage.getUserNameInput().sendKeys(UserName);
		waitForElementVisibility(loginPage.getPasswordInput());
		clickjs(loginPage.getPasswordInput());
		loginPage.getPasswordInput().sendKeys(Password);
		clickjs(loginPage.getloginButton());
	}

}