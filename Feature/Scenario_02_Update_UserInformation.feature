Feature: Update guest information

  Background: logout if already logged in
    Given User logs out if already logged in

  @RegressionTest
  Scenario Outline: To update the guest first name and last name
    Given User is in fashionette home page
    When I login using using "<UserName>" and "<Password>"
    And I navigate to personal data section
    And I click on edit under personal guest details section
    And I update the guest "<firstName>" and "<lastName>"
    And I click on save under personal guest details section
    And I logout after updating the guest information
    And I login using using "<UserName>" and "<Password>"
    And I navigate to personal data section
    Then Validate the updated "<firstName>" and "<lastName>" are saved

    Examples: 
      | firstName | lastName | UserName          | Password        |
      | Canker    | Baser    | QA@fashionette.de | !8Ntr*BM@!#G3VH |
