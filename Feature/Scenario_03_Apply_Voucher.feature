Feature: Apply voucher

  @RegressionTest
  Scenario Outline: Apply voucher
    Given User is in fashionette home page
    When I add "<Product>" to the cart from "<Section>"
    And I navigate to cart
    When I apply "<voucher>" for "<Amount>"
    Then Verify if the discount "<Amount>" is applied to the total

    Examples: 
      | Section  | Product | voucher     | Amount |
      | Handbags | Prada   | qachallenge |      2 |
